import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  public isLogin: string;
  constructor(
    protected router: Router
  ) { }

  ngOnInit(): void {
    this.isLogin = localStorage.getItem('isLoggedIn');
    console.log(localStorage.getItem('isLoggedIn'));
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(localStorage.getItem('isLoggedIn'));
    this.isLogin = localStorage.getItem('isLoggedIn');
  }
    
  scrollToElement(elId: string): void {
    this.router.navigate(['/home-page'], {fragment: elId});
  }

  public logout() {
    localStorage.setItem('isLoggedIn', 'false');
    this.router.navigate(['/login']);
    localStorage.setItem('currentUser', "");
    
  }
}
