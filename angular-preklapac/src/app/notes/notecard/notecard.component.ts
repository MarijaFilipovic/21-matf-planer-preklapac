import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Renderer2, ElementRef } from '@angular/core';

@Component({
  selector: 'app-notecard',
  templateUrl: './notecard.component.html',
  styleUrls: ['./notecard.component.css']
})
export class NotecardComponent implements OnInit {
  @Input()
  title: string;

  @Input()
  description: string;

  @Input()
  link: string;

  @Output('delete')//tipa void jer ne prosledjujemo nista
  deleteEvent: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild('truncator')
  truncator: ElementRef<HTMLElement>;

  @ViewChild("bodyText")
  bodyText: ElementRef<HTMLElement>;

  

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
      /*ako ima mnogo teksta u prozoru za belesku, element truncator ce napraviti fade
    efekat za taj tekst
    ali hocemo da se ovo desava samo ako zaista ima dosta teksta, a ne uvek.
    treba da vidimo da li postoji text overflow, zato hvatamo stil teksta iz tela*/

    /*window.getComputedStyle() vraca objekat koji sadrzi vrednoti svih css svojstava elementa*/
    if(this.bodyText !== undefined){//samo ako ima teksta uopste
      let style = window.getComputedStyle(this.bodyText.nativeElement, null);
      let viewableHeight = parseInt(style.getPropertyValue("height"), 10);

      if(this.bodyText.nativeElement.scrollHeight > viewableHeight){
        //ako se desi text overflow, prikazi truncator
        this.renderer.setStyle(this.truncator.nativeElement, 'display', 'block');
      } else {//ako ne prelazi visinu elementa, onda sakrij truncator
        this.renderer.setStyle(this.truncator.nativeElement, 'display', 'none');
      }
    }
  }

  onXButtonClick(){
    this.deleteEvent.emit();
  }

}
