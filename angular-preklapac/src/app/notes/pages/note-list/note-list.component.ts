import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Note } from '../../shared/note.model';
import { NotesService } from '../../shared/notes.service';
import { Subscription, Observable } from 'rxjs';
import { RefreshSignalService } from '../../shared/refresh-signal.service';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit, OnDestroy {

  public notes: Observable<Note[]>;
  signalSubscription: Subscription;
  
  /**Signal salje noteDetailsComponent kako bi se NoteList refreshovala */
  constructor(private notesService: NotesService,
    private signalService: RefreshSignalService) {
      if(localStorage.getItem('isLoggedIn') === "true"){
        this.notes = this.notesService.getAll();
        
        this.signalSubscription = this.signalService.getSignal()
        .subscribe(sig => {
          if(sig){
            this.notes = this.notesService.refreshNotes();
          }
        });
    }
  }

  ngOnInit(): void {
    
  }

  ngOnDestroy(){
    this.notesService.subsArray.forEach(sub => {
      sub.unsubscribe();
    });

    this.signalSubscription.unsubscribe();
  }

  deleteNote(_id: string){
    this.notesService.subsArray.push(this.notesService.delete(_id).subscribe((res)=>{
      this.notes = this.notesService.refreshNotes();
    }));
  }


}
