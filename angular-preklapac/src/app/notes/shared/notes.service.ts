import { Injectable } from '@angular/core';
import { Note } from './note.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoginRegisterService } from '../../services/login-register-service.service';
import { LoginPageComponent } from '../../login-page/login-page.component';
import { Observable, Subscription } from 'rxjs';
import { HttpErrorHandler} from '../../services/httpErrorHandler.model';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { AuthGuardGuard } from 'src/app/guards/auth-guard.guard';

@Injectable({
  providedIn: 'root'
})
export class NotesService extends HttpErrorHandler{
  //sad ce biti tok a ne niz
  private notes: Observable<Note[]>;

  private readonly notesUrl = "http://localhost:3004/notes/";

  //indeks tekuceg ulogovanog studenta
  private index;

  public subsArray: Subscription[] = new Array<Subscription>();

  constructor(private http: HttpClient, router: Router) {
    super(router);

    if(localStorage.getItem('isLoggedIn') === "true"){
      this.refreshNotes();
    }
  }

  refreshNotes(): Observable<Note[]>{
    if(localStorage.getItem('isLoggedIn') === "true"){
      this.index = JSON.parse(localStorage.getItem('currentUser')).indeks;
    }
    //podesavanje parametra zahteva
    let params = new HttpParams().set('index', this.index);

    this.notes =  this.http.get<Note[]>(this.notesUrl, {'params': params})
    .pipe(
      catchError(super.handleError())
    );
    
    return this.notes;
  }

  getAll(): Observable<Note[]>{
    return this.notes;
  }


  add(formData: any): Observable<Note>{
   
    const body = {
      ...formData, index: this.index
    };
    return this.http.post<Note>(this.notesUrl + 'new_note', body)
    .pipe(
      catchError(super.handleError())
    );
  }

  //_id se ovde odnosi na _id iz baze
  update(_id: string, title: string, description: string): Observable<Note>{
    
    let body = {
      index: this.index,
      title: title,
      description: description,
      note_id: _id
    }
    return this.http.post<Note>(this.notesUrl + 'update_note', body)
    .pipe(
      catchError(super.handleError())
    );
  }

  delete(_id: string){   
    let params = new HttpParams()
    .set('note_id', _id);
    return this.http.delete(this.notesUrl + 'delete_note', {'params': params})
    .pipe(
      catchError(super.handleError())
    );
  }

}
