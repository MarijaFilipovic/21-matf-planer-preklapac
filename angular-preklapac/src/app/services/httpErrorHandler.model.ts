import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

export abstract class HttpErrorHandler{
    constructor(private router: Router){

    }

    protected handleError(){
        return (err: HttpErrorResponse): Observable<never> => {
            if(err.error instanceof ErrorEvent){
            //angular greske - ispis u konzoli
                console.error('An error occured: ', err.error.message);
            } else {
            //greske sa servera - redirekcija na stranicu greske
            //http://localhst:4200/error;message=...;statusCode=... 
                const statusCode = err.status;
                const message = err.message;
                this.router.navigate(['/error', {statusCode, message}]);
            }
            return throwError('Something went wrong; Please, try again later...');
        }; 
    }
}