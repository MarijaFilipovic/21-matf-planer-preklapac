import { Ispit } from './../models/ispit.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KalendarService {

  private ispiti: Observable<Ispit[]>;
  private readonly kalendarUrl = "http://localhost:3004/kalendar/";
  private indeks;
  private ispit: Observable<Ispit>;

  constructor(private http:HttpClient) { 
    if(localStorage.getItem('isLoggedIn') === "true"){
      this.refreshKalendar();
    }
  }

  public refreshKalendar():Observable<Ispit[]>
  {
    if(localStorage.getItem('isLoggedIn') === "true"){
      this.indeks = JSON.parse(localStorage.getItem('currentUser')).indeks;
    }
    console.log("Dohvatanje ispita");
    let params = new HttpParams().set('indeks', this.indeks);

    this.ispiti = this.http.get<Ispit[]>(this.kalendarUrl,{'params':params});
    this.ispiti.forEach(ispiti => {
      ispiti.forEach(n => console.log("Naziv: " + n.naziv + " Datum:" + n.datum));
    });
    
    return this.ispiti;
  }

  public getKalendar():Observable<Ispit[]>
  {
    return this.ispiti;
  }

  public getIspitByNameAndDate(naziv:string,datum:string):Observable<Ispit>
  {
    let params = new HttpParams().set('indeks', this.indeks);
    params.append("naziv",naziv);
    params.append("datum",datum);

    this.ispit = this.http.get<Ispit>(this.kalendarUrl+"/naziv",{'params':params});
    
    return this.ispit;
  
  }


  public add(naziv: string,datum:string,vreme:number): Observable<Ispit>{
      const body = {
          naziv: naziv,
          datum: datum,
          vreme: vreme,
          indeks:this.indeks
      };
      return this.http.post<Ispit>(this.kalendarUrl,body);
  }

  public delete(_id:string)
  {
    let params = new HttpParams().set('indeks', this.indeks).append("_id",_id);
    
    return this.http.delete(this.kalendarUrl,{'params':params});
  }

  public change(_id:string,datum:string,vreme:string)
  {
    const body={
      datum:datum,
      vreme:vreme
    };
    let params = new HttpParams().set('indeks', this.indeks).append("_id",_id);
    
    return this.http.put(this.kalendarUrl,body,{'params':params});
  }

}