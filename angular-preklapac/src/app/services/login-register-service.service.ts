import { User } from './../models/user';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginRegisterService {

  public url = 'http://localhost:3004/students/';
  constructor(
    protected http: HttpClient
  ) { }


  public registerUser(user: any): Observable<any> {
    return this.http.post(this.url, user);
  }
  public loginUser(): Observable<any> {
    return this.http.get(this.url);
  }

  //uzima se jedan konkretan korisnik sa ovim indeksom iz baze
  public getUserByIndex(index: string, pass: string): Observable<User>{
    let body = {
      indeks: index,
      lozinka: pass
    };
   
    return this.http.post<User>(this.url + "log", body);
  }

  public changeUserPassword(index: string, pass: string, newPass: string){
    let body = {
      indeks: index,
      lozinka: pass,
      novaLozinka: newPass
    };
   
    return this.http.post<User>(this.url + "changePass", body);
  }

  public deleteUser(index: string){
    let params = new HttpParams()
    .set('indeks', index);

    return this.http.delete(this.url + 'deleteAccount', {'params': params});
  }

}
