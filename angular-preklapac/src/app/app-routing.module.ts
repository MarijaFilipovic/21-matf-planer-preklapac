import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { KalendarComponent } from './kalendar/kalendar.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RasporedComponent } from './raspored/raspored.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { MainLayoutComponent } from './notes/pages/main-layout/main-layout.component';
import { NoteListComponent } from './notes/pages/note-list/note-list.component';
import { NoteDetailsComponent } from './notes/pages/note-details/note-details.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { ExamGradeListComponent } from './exam-grade-list/exam-grade-list.component';


const routes: Routes = [
   { path: 'raspored', component: RasporedComponent},
   { path: 'login', component: LoginPageComponent},
   { path: '', redirectTo: 'login', pathMatch: 'full'},

   { path: 'home-page', component: HomePageComponent},
   {path: 'home-page/new_note', component: NoteDetailsComponent},
   {path: 'home-page/:id', component: NoteDetailsComponent},

   
   { path: 'kalendar', component: KalendarComponent},

   {path: 'error', component: ErrorPageComponent},
   {path: 'account', component: MyAccountComponent},

   {path: 'examGrade', component: ExamGradeListComponent}

];

@NgModule({

    imports: [RouterModule.forRoot(routes,{
        anchorScrolling: 'enabled'
      })],
    exports: [RouterModule]
})

export class AppRoutingModule{}
