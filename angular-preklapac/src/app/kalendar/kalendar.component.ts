import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { element } from 'protractor';
import { async } from '@angular/core/testing';
import  dayGridPlugin from '@fullcalendar/daygrid';
import { Ispit } from './../models/ispit.model';
import { observable, Observable, Subscription } from 'rxjs';
import { KalendarService } from './../services/kalendar.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular';
import { Calendar } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import { stringify } from '@angular/compiler/src/util';
import { sign } from 'crypto';

@Component({
  selector: 'app-kalendar',
  templateUrl: './kalendar.component.html',
  styleUrls: ['./kalendar.component.css']
})
export class KalendarComponent implements OnInit, OnDestroy {

  public ispiti : Observable<Ispit[]>;
  public noviNiz :{title:string,date:string}[] =[] ;
  public formularZaUnos:FormGroup;
  public isClickedAdd = false;
  public rucniUnos=false;
  public brisanje=false;
  public datumZaUnos:string;
  public izmena = false;
  public otvoriFormularZaIzmenu=false;
  public calendarOptions : CalendarOptions ={};

  
  signalSubscription: Subscription;



  constructor(private kalendarService:KalendarService,
              private formBuilder:FormBuilder) {
    this.formularZaUnos = this.formBuilder.group({
      title: [''],
      date: [''],
      time: ['',[Validators.required]]
    });
    
    this.ispiti=kalendarService.getKalendar();

    this.signalSubscription = this.ispiti.subscribe(ispiti =>
      {
        ispiti.forEach(el=>{
          this.noviNiz.push({title : el.naziv,date : el.datum});
          
      });
      this.calendarOptions = {
          plugins: [interactionPlugin,dayGridPlugin],
          initialView: 'dayGridMonth',
          dateClick: this.handleDate.bind(this),
          events:this.noviNiz
         }; 
      
      });
    
  }
    
 
  
  public submitForm(data){

    if(!this.formularZaUnos.valid){
      window.alert('Nisu popunjena sva polja / Sva polja nisu validna!');
      return;
    }
    if(this.isClickedAdd==true){

      this.isClickedAdd=false;
      const naziv = this.formularZaUnos.controls.title.value;
      const vreme = this.formularZaUnos.controls.time.value;

      this.noviNiz.push({title:naziv,date:this.datumZaUnos});
      //ovim osvezavam kalendar i prikazuje se uneto na html stranici
      this.noviNiz = this.noviNiz.filter(e=> true);
      this.calendarOptions = {
        plugins: [interactionPlugin,dayGridPlugin],
        initialView: 'dayGridMonth',
        dateClick: this.handleDate.bind(this),
        events:this.noviNiz
      }; 

     
    
      this.signalSubscription = this.kalendarService
      .add(naziv,this.datumZaUnos,vreme)
      .subscribe(res => {
        this.ispiti = this.kalendarService.refreshKalendar();
        
      });


      
      this.formularZaUnos.reset();
      
    }
  }

  unosSaDatumom(data)
  {
    if(!this.formularZaUnos.valid){
      window.alert('Nisu popunjena sva polja');
      return;
    }

    this.rucniUnos=false;
   
      
    const naziv = this.formularZaUnos.controls.title.value;
    const vreme = this.formularZaUnos.controls.time.value;
    const datum = this.formularZaUnos.controls.date.value;


      
    this.signalSubscription= this.kalendarService.add(naziv,datum,vreme).subscribe((res)=>{
        this.ispiti = this.kalendarService.refreshKalendar();
    });
    this.noviNiz.push({title:naziv,date:datum});

    //ovim osvezavam kalendar i prikazuje se uneto na html stranici
    this.noviNiz = this.noviNiz.filter(e=> true);  
    
    this.calendarOptions = {
        plugins: [interactionPlugin,dayGridPlugin],
        initialView: 'dayGridMonth',
        dateClick: this.handleDate.bind(this),
        events:this.noviNiz
      }; 
       
    this.formularZaUnos.reset();
    
  }

  obrisi(datum:string)
  {
    //podrazumevace se da je za jedan datum zakazan jedan ispit
    this.brisanje=false;
    this.rucniUnos=false;
    this.isClickedAdd=false;
   
    this.signalSubscription= this.ispiti.subscribe(ispiti => {
      ispiti.find(el=> { 
        
        if(el.datum==datum){
            this.signalSubscription= this.kalendarService.delete(el._id).subscribe((res)=>{
              this.ispiti = this.kalendarService.refreshKalendar();
              
            });
        }
      });
    });
    this.noviNiz = this.noviNiz.filter(el => el.date!=datum);
    this.calendarOptions = {
      plugins: [interactionPlugin,dayGridPlugin],
      initialView: 'dayGridMonth',
      dateClick: this.handleDate.bind(this),
      events:this.noviNiz
     }; 
    
    
  }
     

  public izmeni(datum:string)
  {
    if(!this.formularZaUnos.valid){
      window.alert('Nisu popunjena sva polja');
      return;
    }
    this.otvoriFormularZaIzmenu=false;
    this.izmena=false;
    const novoVreme = this.formularZaUnos.controls.time.value;
    const noviDatum = this.formularZaUnos.controls.date.value;
    

    this.signalSubscription = this.ispiti.subscribe(ispiti => {
      ispiti.find(el => { 
            
          if(el.datum==datum){
             
          
              this.signalSubscription = this.kalendarService.change(el._id,noviDatum,novoVreme).subscribe((res)=>{
                this.ispiti = this.kalendarService.refreshKalendar();
               
              });
              this.noviNiz.push({title:el.naziv,date:noviDatum});
              this.noviNiz = this.noviNiz.filter(el => el.date!=datum);
              this.calendarOptions = {
                plugins: [interactionPlugin,dayGridPlugin],
                initialView: 'dayGridMonth',
                dateClick: this.handleDate.bind(this),
                events:this.noviNiz
               }; 
          
            }
          }
      );
      this.formularZaUnos.reset();
    });


        
  }
  
 
  
  ngOnInit(): void {

  
  }

  
  public handleDate(arg){
    this.datumZaUnos=arg.dateStr;
    if(this.brisanje==true){
      this.obrisi(this.datumZaUnos);
    
    }
    else if(this.izmena==true)
    {
      this.formularZaUnos.reset();
      this.otvoriFormularZaIzmenu=true;
      
    }
    else{
      this.isClickedAdd=true; 
     
    }
  }

  ngOnDestroy(){

    this.signalSubscription.unsubscribe();
  }

  
}