import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from '../models/user';
import { LoginRegisterService } from '../services/login-register-service.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  
  user: User = new User();


  /**Bilo da se student upravo registrovao ili da vec ima nalog, ovde se vrsi uzimanje njegovih podataka */
  constructor(private router: Router) {
    if(localStorage.getItem('isLoggedIn') === "true"){
      this.user = JSON.parse(localStorage.getItem('currentUser'));
    }
  }

  ngOnInit(): void {
  }

  public logOut(){
    localStorage.setItem('isLoggedIn', 'false');
    this.router.navigate(['/login']);
    localStorage.setItem('currentUser', "");
    //localStorage.removeItem('student');
  }

  scrollToElement($element): void {
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }


}
