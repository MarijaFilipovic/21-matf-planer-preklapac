
const mongoose = require('mongoose');

const studentSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  indeks: {
    type: String,
    required: true,
  },
  lozinka: {
    type: String,
    required: true,
  },
  ime: {
    type: String,
    required: false
  },
  prezime: {
    type: String,
    required: false
  }
});


module.exports = mongoose.model('studenti', studentSchema);
