
const mongoose = require('mongoose');

const examGradeSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  subject: {
    type: String,
    required: false
  },
  testPoints: {
    type: Number,
    required: false
  },
  examPoints: {
      type: Number,
      required: false
  },
  grade: {
    type: Number,
    required: false
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,//tip je id jednog studenta
    ref: 'studenti',
    required: true
    }
});


module.exports = mongoose.model('examGrade', examGradeSchema);
