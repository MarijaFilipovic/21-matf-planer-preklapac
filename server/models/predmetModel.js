
const mongoose = require('mongoose');

const predmetSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  ime: {
    type: String,
    required: true,
  },
  ucionica: {
     type: String,
     require: true
  },
  termin: {
      type: String,
      require: true
  }
});


module.exports = mongoose.model('predmet', predmetSchema);