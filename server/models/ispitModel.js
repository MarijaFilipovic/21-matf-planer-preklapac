const mongoose = require('mongoose');

const ispitSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    naziv: {
        type: String,
        required: true
    },
    datum: {
        //prosledjuje se u JSONU 
        //"YYYY-MM-DD"
        type: String,
        required: true
    },
    vreme: {
        type: Number,
        required: true
    },
    indeks: {
        type: String,
        red: 'studenti',
        required: true
    }

});


module.exports = mongoose.model('ispit', ispitSchema);