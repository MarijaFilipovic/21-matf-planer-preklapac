const mongoose = require('mongoose');

const smerPredmetModelSchema =  new mongoose.Schema({

    _id : mongoose.Schema.Types.ObjectId,
    smer: {
        type: String,
        required: true

    },
    predmet: {
        type: String,
        required: true
    },
    predmeti: [{
        ime: {
          type: String,
          required: true,
        },
        ucionica: {
           type: String,
           require: true
        },
        termin: {
            type: String,
            require: true
        }
      }]
   
});

module.exports = mongoose.model('smerPredmet', smerPredmetModelSchema);