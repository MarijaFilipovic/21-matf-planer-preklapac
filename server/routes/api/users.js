const express = require('express');
const router = express.Router();


const controllerUser = require('../../controllers/usersController');
const controllerRaspored = require('../../controllers/rasporedController');

// http://localhost:3000/students
router.post('/log', controllerUser.getStudentByIndex);
router.get('/', controllerUser.getStudents);
router.post('/', controllerUser.createStudent);
router.post('/changePass', controllerUser.changePasswordByIndex);
router.delete('/deleteAccount', controllerUser.deleteAccount);

// http://localhost:3000/students/predmet
//get koji vraca smer - predmet iz baze 
router.get('/predmet', controllerRaspored.getSmerPredmet);
router.post('/predmet', controllerRaspored.createSmerPredmet);

// http://localhost:3000/students/raspored
router.get('/raspored/:indeks', controllerRaspored.getRasporedByIndeks);
router.post('/raspored', controllerRaspored.createRaspored);




module.exports = router;
