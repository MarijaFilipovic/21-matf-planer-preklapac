const express = require('express');
const router = express.Router();

const notesController = require("../../controllers/notesController");

//http://localhost:3004/notes/
router.post('/new_note', notesController.createNote);
router.get('/', notesController.getNotes);
router.post('/update_note', notesController.updateByNoteId);
router.delete('/delete_note', notesController.deleteByNoteId);

module.exports = router;