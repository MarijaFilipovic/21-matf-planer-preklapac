const express = require('express');
const { urlencoded, json } = require('body-parser');
const mongoose = require('mongoose');
var cors = require('cors')

const app = express();

const databaseString = 'mongodb://localhost:27017/preklapac';
mongoose.connect(databaseString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});


mongoose.connection.once('open', function () {
  console.log('Uspesno povezivanje!');
});

mongoose.connection.on('error', (error) => {
  console.log('Greska: ', error);
});

app.use(json());
app.use(cors());
app.use(urlencoded({ extended: false }));


const usersRoutes = require('./routes/api/users');
const notesRoutes = require('./routes/api/notes');
const examGradeRoutes = require('./routes/api/examGrade');

app.use('/students', usersRoutes);
app.use('/notes', notesRoutes);
app.use('/examGrade', examGradeRoutes);

const kalendarRoutes = require('./routes/api/kalendar');
app.use('/kalendar',kalendarRoutes);

app.use(function (req, res, next) {
  const error = new Error('Zahtev nije podrzan!');
  error.status = 405;

  next(error);
});

app.use(function (error, req, res, next) {
  const statusCode = error.status || 500;
  res.status(statusCode).json({
    error: {
      message: error.message,
      status: statusCode,
      stack: error.stack,
    },
  });
});
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3008');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});



module.exports = app;