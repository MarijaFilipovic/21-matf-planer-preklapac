const mongoose = require('mongoose');
const Raspored = require('../models/rasporedModel');
const Predmet = require('../models/predmetModel.js');
const smerPredmet = require('../models/smerPredmetModel');

const http = require('http');
const cheerio = require('cheerio');
const cheerioTableparser = require('cheerio-tableparser');

module.exports.createRaspored = async (req, res, next) => {
    if (!req.body.indeks || !req.body.predmeti ) {
      res.status(400);
      res.send();
    } else {
      try {
        const proveraRaspored = await Raspored.find({indeks : req.body.indeks}, {predmeti: 0}).exec();
       
        if (!proveraRaspored.length){
          const newRaspored = new Raspored({
            _id: new mongoose.Types.ObjectId(),
            indeks: req.body.indeks,
            predmeti: req.body.predmeti,

          });
          
          await newRaspored.save();
          return res.status(201).json(newRaspored);
        }else{
          

          const newRaspored = await Raspored.update({indeks: req.body.indeks}, {predmeti: req.body.predmeti}).exec();
          return res.status(201).json(newRaspored);
         /* await Raspored.deleteMany({indeks: req.body.indeks});
          const newRaspored = new Raspored({
            _id: new mongoose.Types.ObjectId(),
            indeks: req.body.indeks,
            predmeti: req.body.predmeti,

          });
          await newRaspored.save();
          return res.status(201).json(newRaspored);*/
        }

      } catch (err) {
        next(err);
      }
    }
  };

  module.exports.getRasporedByIndeks = async (req, res, next) => {
    const ind = req.params.indeks;
    try {
        const rasporedInstanca = await Raspored.find({indeks: ind}).exec();
        
        if (!rasporedInstanca) {
          return res
            .status(404)
            .json({ message: 'Ne postoji student sa ovim indeksom' });
        }
        res.status(200).json(rasporedInstanca);
      } catch (err) {
        next(err);
      }
  };

  /*module.exports.getPredmetFromMatfHTML = async (req, res, next) => {
    console.log("Usli smo u getPredmetFrom");
   
    var options = {

      host: 'poincare.matf.bg.ac.rs',

      path: '/~kmiljan/raspored/sve/form_005.html'

    };

    
    
    http.get(options, function(response) {

        // initialize the container for our data

        var data = "";
        // this event fires many times, each time collecting another piece of the response

        response.on("data", function (chunk) {
            // append this chunk to our growing `data` var
            data += chunk;
        });
        // this event fires *one* time, after all the `data` events/chunks have been gathered
        response.on("end", function () {

            // you can use res.send instead of console.log to output via express

           
            
            const $ = cheerio.load(data);
        let predmetstring = 'Анализа 1';

        
       
         $('td').filter(function(){
          
          let karma = new String($(this).text()) ;
          if(karma.includes(predmetstring))
          {console.log($(this).text());
          return true;}
          else
          return false;
          

        });
        
        
        });

        
        
        
        
        
        
        //cheerioTableparser($);
        // *****Ovde bi trebalo raditi parsiranje html-a kako i popuniti response*****
        // 
        
    }).on('error', function(e) {

      console.log("Got error: " + e.message);

    });
   // console.log("*******************************************");
   // console.log("Pomocna" + pomocna);
   //Ostavljen je kod koji se nalazi i u getRasporedByIndeks
   //Poenta ovoga bilo je debagovanje prethodnog koda i console.log-a
   // Ostavljam ga ovde kako bi se lakse pratio tekst html-a koji dovlacimo
    const ind = req.params.indeks;
    try {
        const rasporedInstanca = await Raspored.find({indeks: ind}, {indeks: 0}).exec();
        if (!rasporedInstanca) {
          return res
            .status(404)
            .json({ message: 'Ne postoji student sa ovim indeksom' });
        }
        res.status(200).json(rasporedInstanca);
      } catch (err) {
        next(err);
      }
  };*/

module.exports.getSmerPredmet = async (req, res, next) => {
  try {

    const stud = await smerPredmet.findOne({smer: req.query.smer, predmet: req.query.predmet} ).exec();
    console.log(stud);
    console.log(req.query.predmet + " " + req.query.smer);
    res.status(200).json(stud);
    
  } catch (err) {
    next(err);
  }
};

module.exports.createSmerPredmet = async (req, res, next) => {
  if (!req.body.smer || !req.body.predmet || !req.body.predmeti   ) {
    
    res.status(400);
    res.send();
  } else {
    try {
      const proverasmerPredmet = await smerPredmet.find({smer : req.body.smer}, {predmeti: req.body.predmet}).exec();
       
    if (!proverasmerPredmet.length){
      const newsmerPredmet = new smerPredmet({
        _id: new mongoose.Types.ObjectId(),
        smer : req.body.smer,
        predmet: req.body.predmet,
        predmeti: req.body.predmeti,

      });
      await newsmerPredmet.save();
      return res.status(201).json(newsmerPredmet);
    }else{
      const newsmerPredmet = await smerPredmet.update({smer : req.body.smer,},{predmet: req.body.predmet}, {predmeti: req.body.predmeti}).exec();
      return res.status(202).json(newsmerPredmet);
    }
     /* console.log("logujem");
      const newSmerPredmet = new smerPredmet({
        _id: new mongoose.Types.ObjectId(),
        smer: req.body.smer,
        predmet: req.body.predmet,
        predmeti: req.body.predmeti
      });
      console.log(newSmerPredmet);
      await newSmerPredmet.save();

      res.status(201).json(newSmerPredmet); */
    } catch (err) {
      next(err);
    }
  }
};